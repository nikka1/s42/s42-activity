const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

const txtLastName = document.querySelector("#txt-last-name");


function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}

txtFirstName.addEventListener('keyup', printFullName);
txtLastName.addEventListener('keyup', printFullName);